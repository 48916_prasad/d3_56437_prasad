const { response } = require('express')
const express = require ('express')
const { request } = require('https')
const database = require('mime-db')
const mysql =require('mysql2')
const cors =require('cors')

const app=express()
app.use(cors('*'))
app.use(express.json())

const openDatabaseConnection =()=>{

    const connection = mysql.createConnection({
        port:3306,
      
        host:'localhost',
        user:'root',
        password: 'manager',
        database : 'db'
    })
    connection.connect()
    return connection

} 

app.post ('/add', (request,response)=>{
    const connection =openDatabaseConnection()
    const {movie_id, movie_title, movie_release_date,movie_time,director_name}=request.body
    const statement=`insert into movie (movie_id, movie_title, movie_release_date,movie_time,director_name) values (${movie_id}, '${movie_title}', '${movie_release_date}','${movie_time}','${director_name}')`
    connection.query (statement,(error,result)=>{
        connection.end
        if (error){
            response.send(error)
        }
        else {
            response.send(result)
        }
    })
})


app.get ('/display', (request,response)=>{
    const connection =openDatabaseConnection()
    const { movie_title}=request.body
    const statement=`select * from movie where movie_title = '${movie_title}'`
    connection.query (statement,(error,result)=>{
        connection.end
        if (error){
            response.send(error)
        }
        else {
            response.send(result)
        }
    })
})


app.put ('/edit', (request,response)=>{
    const connection =openDatabaseConnection()
    const {movie_id, movie_release_date,movie_time}=request.body
    const statement=`update movie set movie_release_date ='${movie_release_date}' , movie_time = '${movie_time}' where movie_id = ${movie_id}`
    connection.query (statement,(error,result)=>{
        connection.end
        if (error){
            response.send(error)
        }
        else {
            response.send(result)
        }
    })
})

app.delete ('/delete', (request,response)=>{
    const connection =openDatabaseConnection()
    const {movie_id}=request.body
    const statement=`delete from movie where movie_id = ${movie_id}`
    connection.query (statement,(error,result)=>{
        connection.end
        if (error){
            response.send(error)
        }
        else {
            response.send(result)
        }
    })
})

app.listen(4000, '0.0.0.0', ()=>{
    console.log(`server started on port 4000`)
})